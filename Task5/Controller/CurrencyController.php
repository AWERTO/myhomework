<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 06.05.2018
 * Time: 11:14
 */

class CurrencyController extends Controller
{
    const API = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';
    protected function getJson() {
        $bank = file_get_contents(self::API);
        $bank = json_decode($bank);
        return $bank;
    }

    public function indexAction(){
        foreach ($this->getJson() as $item) {
            if ($item->ccy === "USD") {
                $BuyUSD = $item->buy;
                $SellUSD = $item->sale;
            }
            if ($item->ccy === "EUR") {
                $BuyEUR = $item->buy;
                $SellEUR = $item->sale;
            }
            if ($item->ccy === "BTC") {
                $BuyBTC = $item->buy;
                $SellBTC = $item->sale;
            }
            if ($item->ccy === "RUR") {
                $BuyRUR = $item->buy;
                $SellRUR = $item->sale;
            }
        }
        return $this->render('index', [
            'BuyUSD' => $BuyUSD,
            'SellUSD' => $SellUSD,
            'BuyEUR' => $BuyEUR,
            'SellEUR' => $SellEUR,
            'BuyRUR' => $BuyRUR,
            'SellRUR' => $SellRUR,
            'BuyBTC' => $BuyBTC,
            'SellBTC' => $SellBTC,
        ]);
    }


    public function calculateAction(Request $request) {
        if (!$request->isPost()) {
            return false;
        }

        $form1 = $request->post('form1');
        $form2 = $request->post('form2');
        $ish = $request->post('ish');

        foreach ($this->getJson() as $item) {
            if ($item->ccy === "USD") {
                $BuyUSD = $item->buy;
                $SellUSD = $item->sale;
            }
            if ($item->ccy === "EUR") {
                $BuyEUR = $item->buy;
                $SellEUR = $item->sale;
            }
            if ($item->ccy === "BTC") {
                $BuyBTC = $item->buy;
                $SellBTC = $item->sale;
            }
            if ($item->ccy === "RUR") {
                $BuyRUR = $item->buy;
                $SellRUR = $item->sale;
            }
        }
        return header('Location: ?route=currency/converter');
    }
}