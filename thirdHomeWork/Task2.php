<?php
class Calculator {
    public $firstArguments;
    public $secondArguments;
    public function __construct($firstArguments, $secondArguments)
    {
        $this->firstArguments = $firstArguments;
        $this->secondArguments = $secondArguments;
    }
    function add(){
        return $this->firstArguments+$this->secondArguments;
    }
    function subtract(){
        return $this->firstArguments-$this->secondArguments;
    }
    function divide(){
        return $this->firstArguments/$this->secondArguments;
    }
    function multiply(){
        return $this->firstArguments*$this->secondArguments;
    }
}
$mycalc = new Calculator( 12, 6);

echo $mycalc->add(); // Displays 18
echo ("</br>");
echo $mycalc->multiply(); // Displays 72