<?php

class  FormBuilder {

    public function open($arr) {
        $formOpen = "<form";
        foreach ($arr as $value=>$key)
            $formOpen .= (' '.$key.'='.'"'.$value.'"');
        $formOpen .= '>';
        return $formOpen;
    }
    public function input($arr){
        $inputTag = '<input';
        foreach ($arr as $key=>$value)
            $inputTag .= (' '.$key.'='.'"'.$value.'"');
        $inputTag .= '>';
        return $inputTag;
    }

    public function submit($arr){
        $buttonTag='<button type="submit">';
        foreach ($arr as $value)
            $buttonTag .= $value;
        $buttonTag .= '</button>';
        return $buttonTag;
    }

    public function close(){
        $formCloseTag='</form>';
        return $formCloseTag;
    }
}

?>