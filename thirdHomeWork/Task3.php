<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
include 'Task3_1.php';

$form = new FormBuilder();
echo $form->open(['action'=>'index3.php', 'method'=>'POST']);
echo $form->input(['type'=>'text', 'placeholder'=>'Ваше имя', 'name'=>'name']);
echo $form->input(['type'=>'text', 'placeholder'=>'email', 'name'=>'email']);
echo $form->submit(['value'=>'Отправить']);
echo $form->close();
?>
</body>
</html>
