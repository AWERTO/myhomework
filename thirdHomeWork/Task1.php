<?php
class WorkerAlpha{
    public $name="Ivan";
    public $age=25;
    public $salary=1000;
    public function getName(){
        return $this->name;
    }public function getAge(){
    return $this->age;
}public function getSalary(){
    return $this->salary;
}
}
$firstWorker = new WorkerAlpha();
echo ($firstWorker->getName());
echo ("</br>");
echo ($firstWorker->getAge());
echo ("</br>");
echo ($firstWorker->getSalary());
echo ("</br>");
$secondWorker = new WorkerAlpha();
echo ($firstWorker->name="Vasya");
echo ("</br>");
echo ($firstWorker->age=26);
echo ("</br>");
echo ($firstWorker->salary=2000);
echo ("</br>");
$summ = $firstWorker->salary+$secondWorker->salary ;
echo ("The summ of salary Vasya and Ivan is  $summ ");
echo ("</br>");
echo ("</br>");
echo ("</br>");
echo ("</br>");



class WorkerBeta
{
    private $name;
    private $age;
    private $salary;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function setSalary($salary)
    {
        $this->salary = $salary;
    }
    public function getName(){
        return $this->name;
    }
    public function getAge(){
        return $this->age;
    }
    public function getSalary(){
        return $this->salary;
    }
    function doSomething(){
        echo $this->getName();
        echo ("</br>");
        echo $this->getAge();
        echo ("</br>");
        echo $this->getSalary();
        echo ("</br>");
    }
}
$firstWorker = new WorkerBeta();
$firstWorker->setName("Ivan");
$firstWorker->setAge(25);
$firstWorker->setSalary(1000);
$firstWorker->doSomething();
$secondWorker = new WorkerBeta();
$secondWorker->setName("Vasya");
$secondWorker->setAge(26);
$secondWorker->setSalary(2000);
$secondWorker->doSomething();
$summ = $firstWorker->getSalary()+$secondWorker->getSalary();
echo ("The summ of salary Vasya and Ivan is  $summ ");
echo ("</br>");
echo ("</br>");
echo ("</br>");



class WorkerOmega
{
    private $name;
    private $age;
    private $salary;
    public function checkAge($age){
        if ($age<100 && $age>1){
            return true;
        }else
        {
            return "indefined age, try again";
        }
    }
    public function setName($name)
    {
        $this->name = $name;
    }

    public function setAge($age)
    {
        if ($this->checkAge($age)){
            $this->age = $age;
        }else {
            echo ("Indefined");
        }
    }

    public function setSalary($salary)
    {
        $this->salary = $salary;
    }
    public function getName(){
        return $this->name;
    }
    public function getAge(){
        return $this->age;
    }
    public function getSalary(){
        return $this->salary;
    }
    function doSomething(){
        echo $this->getName();
        echo ("</br>");
        echo $this->getAge();
        echo ("</br>");
        echo $this->getSalary();
        echo ("</br>");
    }
}
$firstWorker = new WorkerOmega();
$firstWorker->setName("Ivan");
$firstWorker->setAge(25);
$firstWorker->setSalary(1000);
$firstWorker->doSomething();
$secondWorker = new WorkerOmega();
$secondWorker->setName("Vasya");
$secondWorker->setAge(26);
$secondWorker->setSalary(2000);
$secondWorker->doSomething();
$summ = $firstWorker->getSalary()+$secondWorker->getSalary();
echo ("The summ of salary Vasya and Ivan is  $summ ");
